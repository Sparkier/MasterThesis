\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}General Problem}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Concept}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Cylindrical Shape Recognition}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Model Integration}{6}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Ingrowth Visualization}{7}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Visualization Concepts}{9}{subsection.1.2.4}
\contentsline {section}{\numberline {1.3}Overview}{12}{section.1.3}
\contentsline {chapter}{\numberline {2}Related Work}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}Implant Ingrowth Research}{15}{section.2.1}
\contentsline {section}{\numberline {2.2}Implant Visualization}{18}{section.2.2}
\contentsline {chapter}{\numberline {3}Realization through the Visualization Pipeline}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Data Acquisition}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}Data Enhancement}{24}{section.3.2}
\contentsline {section}{\numberline {3.3}Data Mapping and Image Rendering}{26}{section.3.3}
\contentsline {chapter}{\numberline {4}Implementation}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}Implant Recognition}{29}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Shape Recognition}{30}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Circle Detection}{32}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Position and Length Estimation}{37}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Implant Orientation and Rotation}{38}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2}Visualization Approach}{43}{section.4.2}
\contentsline {chapter}{\numberline {5}Results}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Circle Detection}{45}{section.5.1}
\contentsline {section}{\numberline {5.2}Position and Length Estimation}{47}{section.5.2}
\contentsline {section}{\numberline {5.3}Cylinder Integration}{48}{section.5.3}
\contentsline {chapter}{\numberline {6}Limitations}{51}{chapter.6}
\contentsline {section}{\numberline {6.1}Implant Variations}{51}{section.6.1}
\contentsline {section}{\numberline {6.2}Scan Quality}{52}{section.6.2}
\contentsline {section}{\numberline {6.3}Implant Shape}{53}{section.6.3}
\contentsline {section}{\numberline {6.4}User Interaction}{53}{section.6.4}
\contentsline {chapter}{\numberline {7}Conclusion and Future Work}{55}{chapter.7}
\contentsline {section}{\numberline {7.1}Conclusion}{55}{section.7.1}
\contentsline {section}{\numberline {7.2}Future Work}{56}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Data Quality}{56}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Implant Model Integration}{57}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Automation}{58}{subsection.7.2.3}
\contentsline {subsection}{\numberline {7.2.4}Implant Shapes}{58}{subsection.7.2.4}
\contentsline {subsection}{\numberline {7.2.5}Visualization Techniques}{59}{subsection.7.2.5}
\contentsline {subsection}{\numberline {7.2.6}Hough Transform for multiple Radii}{60}{subsection.7.2.6}
\contentsline {chapter}{\numberline {A}Source Code}{61}{appendix.A}
\contentsline {section}{\numberline {A.1}GPU Hough Transform}{61}{section.A.1}
\contentsline {chapter}{\nonumberline Bibliography}{63}{chapter*.28}
