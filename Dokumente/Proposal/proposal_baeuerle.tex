\documentclass[a4paper,12pt,abstracton,titlepage]{scrartcl}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} % Umlaute, evtl. vom Betriebssystem abhaengig
\usepackage{lmodern}
\usepackage{pgfgantt}
\usepackage{titlesec}
\usepackage[backend=biber,sortcites,defernumbers]{biblatex}
\addbibresource{bibliography.bib}

\titleformat*{\section}{\large\bfseries}
\titleformat*{\subsection}{\large\bfseries}
\titleformat*{\subsubsection}{\large\bfseries}
\titleformat*{\paragraph}{\large\bfseries}
\titleformat*{\subparagraph}{\large\bfseries}

\begin{document}

%\maketitle

%%% begin costom title
{\Large\noindent \emph{Ulm University}}
\begin{center}
	{\large Visualization of bone growth into implants and comparing the ingrowth with simulation results \\ \large Alex B\"auerle \\ \today}
\end{center}
%%% end custom title

\setcounter{page}{1} % reset page counter to one for the first page, leave the title page out

\section*{Abstract}
The ceramic bone implants used in this context have certain holes for the bone structure to grow into and stabilize the bone. The illustration and appropriate evaluation of this ingrowth is a topic of much interest as it can be used to optimize implants and thus healing processes. Existing simulations are a big help for doctors to illustrate the ingrowth of the bone. However, these simulations have to be checked against real data, which can be collected by a \emph{Computed Tomographic (CT)} scan, to evaluate the correctness of the simulation. The goal of this thesis is to find appropriate illustrations for the bone ingrowth and optionally compare this with simulation results.

\section{Problem, Goal and Approach}\label{goal}
For achieving the final goal of evaluating the simulation results for the bone ingrowth, there are the following tasks that need to be done in this order:
\begin{enumerate}
\item Manage to separate the bone structure of the implants in the illustration
\item Find an illustration that shows the ingrowth in a convenient way
\item Find a way to compare the simulation with real results
\end{enumerate}
For the first task of separating the bone structure from the implant, it is important to know that with the data of the CT-scan, these structures cannot be separated easily. The ceramic of the implant and the bones themselves have a very similar attenuation in CT, so it is hard to distinguish between these two materials. However, for the measurement of the bone ingrowth an accurate separation is essential. To do this separation, I want to extract certain features of the data and locate them in a three-dimensional model of the implant. I will then fit the model into the visualization of the CT-data. After this everything else can assumed to be bone structure and the thin bone layer can be distinguished accurately from the implant.\\
In the second step, the task is to come up with an illustration that gives the doctor a possibility to see how the bone has grown into the implant. Up until now, this is hard to see, because the bone layer on the implant is very thin and the nature of the tree-dimensional pipe structure of the implant makes it hard to evaluate the quality of the ingrowth. The visualization that will be developed should make this evaluation a lot easier.\\
As a last optional step, this visualization should be comparable to the existing simulations that model the bone ingrowth. These simulations can then be verified against the actual data. With these results, it might be possible to refine these simulations to even better fit the actual bone growth, if there appears to be any optimization potential.

\section{Previous Work}
2004, Rebaudi, Koller, Laib et al \cite{Rebaudi2004} used $\mu$CT-scans to analyse the bone-to-implant apposition at an implanted titanium screw. The results were similar to these obtained with histology. In this paper, they did not have ingrowth analysis since the screw was only surrounded by bone and did not have any holes for the bone to grow into. Also they had no problems in separating the titanium screw from the bone structure, because of the titanium being radio-opaque and having no necessity to see into the screw.\\
In a work by Baril, Lefebvre and Hacking \cite{Baril2011} in 2011, the use of $\mu$CT-scans for implant ingrowth analysis proved to be a sufficient method. The advantages of these scans over classical histology based methods are obvious, as they are non destructive and could even be done invivo. However, since they measured the porosity of these implants with a fixed threshold, this method only provides accurate results for a threshold that separates the implant from the bone structure perfectly. In contrast to this, a feature recognition and then fitting of a 3D-model into the data could also work without this threshold and therefore be applicable for multiple implant materials.

\section{Implementation}
The work of this thesis will be done with the \emph{Interactive Visualization Workshop (inviwo)}\cite{inviwo}. The advantages of this are, that a volumetric illustration of the CT-data is already implemented and a modular processor architecture can be utilized. What needs to be built on top of this are processors for recognizing characteristics in the scan, matching the model into the three-dimensional data-volume, generating a appropriate visualization and comparing to the simulation results. For this, the CT-data is needed as well as the three-dimensional models of the implants and the simulation results. The thesis is done in cooperation with the \emph{Institute of Orthopaedic Research and Biomechanics} of \emph{University of Ulm}.

\section{Risks}
To complete all the tasks that were described in Section \ref{goal} is a tough aim. One risk is, that in the time frame of this thesis, not all of the features can be implemented. The focus therefore lies on producing a good visualization of the bone ingrowth. If there is time, the comparison to simulation results will be implemented.\\
A risk right at the beginning is that the feature extraction and the resulting model matching into the CT-data is not possible. The small differences in attenuation between ceramic and bone could mean that it might not be possible to distinguish between these materials. Then no accurate identification of bone structures would be possible. At the same time, if the three-dimensional model of the implant differs from the actual implant, which can come from manufacturing inaccuracy, the bone identification could also lack precision.

\section{Time Schedule}
For a rough task list, the following tasks have to be performed:
\begin{itemize}	
	\item Preparation (Inviwo and development environment setup) (October 2016)
	\item Literature Research (October 2016)
	\item Familiarizing with the data (November 2016)
	\item Development of feature extracting processor and matching a 3D-model into the data (January 2016)
	\item Finding and implementing a visualization for the bone ingrowth (March 2016)
	\item Evaluation of the visualization with the partners of the Institute of Orthopaedic Research and Biomechanics (March 2016)
	\item Implementation of a comparison of the data with simulation results (highly optional)
	\item Writing (April 2017)
\end{itemize}
The dates that have been stated after each item can be seen as estimated finishing dates of each task.

%\renewcommand{\bibname}{\section{Sources}} % Redefine bibname
\printbibliography

\end{document}
